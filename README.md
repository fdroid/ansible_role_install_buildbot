
Ansible role for setting up Buildbot for F-Droid projects.  F-Droid is based on
Debian as much as possible, since Debian provides reproducible builds and a
strong, continuous chain of verification.  So this role installs things from
Debian whenever possible.  Then as this setup stabilizes, we will package all
the dependencies so that they are available from Debian.  This is why there is
currently a hybrid apt/pip install.


## Testing

There is a Vagrant setup in _tests/_ for testing this locally, and it is also
tested via GitLab CI.
